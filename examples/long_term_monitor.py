# -*- coding: utf-8 -*-
"""long_term_monitor.py

This script repeatedly records readouts from the Thorlabs TSP01B sensor.
Requires a connected TSP01 Rev.B sensor.

Author: Dima Pustakhod
Copyright: TU/e
"""
from time import time, sleep, gmtime, strftime

from py_thorlabs_tsp import ThorlabsTsp01B


# Settings
# --------
DURATION_S = 3600 * 24
INTERVAL_S = 60
HEADER = 'time,th0,th1,th2,h0\n'
fname = '_output_data/{} - data.csv'.format(strftime('%Y%m%dT%H%M%S', gmtime()))


def record_sensor_values(sensor: ThorlabsTsp01B, fname: str):
    t = time()
    t1 = sensor.measure_temperature('th0')
    t2 = sensor.measure_temperature('th1')
    t3 = sensor.measure_temperature('th2')
    h = sensor.measure_humidity()

    with open(fname, 'a+') as f:
        f.write('{},{},{},{},{}\n'.format(t, t1, t2, t3, h))


sensor = ThorlabsTsp01B('M00549344')

with open(fname, 'a+') as f:
        f.write(HEADER)

print('\nDO NOT TERMINATE THE PROGRAM! THE MEASUREMENT IS ONGOING!\n')

t = time()
t_end = t + DURATION_S
t_next = t + INTERVAL_S

while t < t_end:
    record_sensor_values(sensor, fname)
    sleep(t_next - time())

    t = time()
    t_next = t + INTERVAL_S

print('Measurement complete.')

sensor.release()


