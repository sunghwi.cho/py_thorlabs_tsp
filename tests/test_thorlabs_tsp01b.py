# -*- coding: utf-8 -*-
"""test_thorlabs_tsp01b.py

This script tests the ThorlabsTsp01B device class.
Requires a connected TSP01 Rev.B sensor.

Author: Dima Pustakhod
Copyright: TU/e

"""
import os
from path import Path
import sys

p = Path(os.getcwd()).parent
if p not in sys.path:
    sys.path.append(p)


from py_thorlabs_tsp import ThorlabsTsp01B
from py_thorlabs_tsp import ThorlabsTsp01BException

# Connect with the first available device
# ---------------------------------------
print('\nTest 1\n' + '-' * 50)
sensor = ThorlabsTsp01B()
sensor.release()

# Connect with the device with the given SN
# -----------------------------------------
print('\nTest 2\n' + '-' * 50)
sensor = ThorlabsTsp01B('M00549344')
sensor.release()

# Try to connect with the device with the wrong SN
# ------------------------------------------------
print('\nTest 3\n' + '-' * 50)
try:
    sensor = ThorlabsTsp01B('M00549345')
except ThorlabsTsp01BException as e:
    print(e)

# Test measure temperature functionality
# --------------------------------------
print('\nTest 4\n' + '-' * 50)
sensor = ThorlabsTsp01B()

print('Internal', sensor.measure_temperature(), 'degC')

for ch_id in ('th0', 'th1', 'th2'):
    print('Ch', ch_id, sensor.measure_temperature(ch_id), 'degC')

sensor.release()

# Test measure humidity functionality
# -----------------------------------
print('\nTest 5\n' + '-' * 50)
sensor = ThorlabsTsp01B()

print('Dflt channel', sensor.measure_humidity(), '%')

for ch_id in ('h0',):
    print('Ch', ch_id, sensor.measure_humidity(ch_id), '%')

sensor.release()
